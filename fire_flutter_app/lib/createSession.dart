// ignore_for_file: file_names

import 'dart:developer';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class CreateSession extends StatefulWidget {
  const CreateSession({Key? key}) : super(key: key);

  @override
  State<CreateSession> createState() => _CreateSessionState();
}

class _CreateSessionState extends State<CreateSession> {
  TextEditingController nameController = TextEditingController();
  TextEditingController gradeController = TextEditingController();
  TextEditingController subjectController = TextEditingController();

  Future _createSessions() {
    final name = nameController.text;
    final grade = gradeController.text;
    final subject = subjectController.text;

    final ref = FirebaseFirestore.instance.collection('Sessions').doc();

    return ref
        .set({"Your name": name, "Grade": grade, "Subject_name": subject})
        .then((value) => log('Collection Created!!'))
        .catchError((onError) => log(onError));
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
          child: TextField(
            controller: nameController,
            decoration: InputDecoration(
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
              hintText: 'Enter your name',
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
          child: TextField(
            controller: gradeController,
            decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
                hintText: 'Enter grade'),
          ),
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
          child: TextField(
            controller: subjectController,
            decoration: InputDecoration(
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
                hintText: 'Enter subject name'),
          ),
        ),
        ElevatedButton(onPressed: () {_createSessions();}, child: const Text('Create session'))
      ],
    );
  }
}
