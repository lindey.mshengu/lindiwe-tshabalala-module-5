import 'package:fire_flutter_app/createSession.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';


Future main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: const FirebaseOptions(
      apiKey: "AIzaSyBKlEhHA8DhUSiOh5RoD2ph8jn86vPKnGM",
      authDomain: "fireflutter-44773.firebaseapp.com",
      projectId: "fireflutter-44773",
      storageBucket: "fireflutter-44773.appspot.com",
      messagingSenderId: "419972152596",
      appId: "1:419972152596:web:cc2922ff7bbc6662469601"
      )
    );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        
        primarySwatch: Colors.deepPurple,
      ),
      home: const MyHomePage(title: 'FireFlutterApp'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);


  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
   
    return Scaffold(
      appBar: AppBar(
       
        title: Text(widget.title),
      ),
      body: Center(
       
        child: Column(
          
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[CreateSession()],
        )
      ),
    );
  }
}
